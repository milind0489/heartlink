// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Login_Activity_ViewBinding implements Unbinder {
  private Login_Activity target;

  @UiThread
  public Login_Activity_ViewBinding(Login_Activity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Login_Activity_ViewBinding(Login_Activity target, View source) {
    this.target = target;

    target.btnfacebook = Utils.findRequiredViewAsType(source, R.id.btn_facebook, "field 'btnfacebook'", Button.class);
    target.btnlogin = Utils.findRequiredViewAsType(source, R.id.btn_login, "field 'btnlogin'", Button.class);
    target.btngoogle = Utils.findRequiredViewAsType(source, R.id.btn_google, "field 'btngoogle'", Button.class);
    target.ivloginlogo = Utils.findRequiredViewAsType(source, R.id.ivloginlogo, "field 'ivloginlogo'", ImageView.class);
    target.evUsername = Utils.findRequiredViewAsType(source, R.id.evUsername, "field 'evUsername'", EditText.class);
    target.textInputLayoutUsername = Utils.findRequiredViewAsType(source, R.id.text_input_layout_username, "field 'textInputLayoutUsername'", TextInputLayout.class);
    target.evPassword = Utils.findRequiredViewAsType(source, R.id.evPassword, "field 'evPassword'", EditText.class);
    target.textInputLayoutPassword = Utils.findRequiredViewAsType(source, R.id.text_input_layout_password, "field 'textInputLayoutPassword'", TextInputLayout.class);
    target.evPhone = Utils.findRequiredViewAsType(source, R.id.evPhone, "field 'evPhone'", EditText.class);
    target.textInputLayoutPhone = Utils.findRequiredViewAsType(source, R.id.text_input_layout_phone, "field 'textInputLayoutPhone'", TextInputLayout.class);
    target.v1Line = Utils.findRequiredView(source, R.id.v1_line, "field 'v1Line'");
    target.tvLine = Utils.findRequiredViewAsType(source, R.id.tv_line, "field 'tvLine'", TextView.class);
    target.tv = Utils.findRequiredViewAsType(source, R.id.tv, "field 'tv'", TextView.class);
    target.checkBoxRememberMe = Utils.findRequiredViewAsType(source, R.id.checkBoxRememberMe, "field 'checkBoxRememberMe'", CheckBox.class);
    target.v2Line = Utils.findRequiredView(source, R.id.v2_line, "field 'v2Line'");
    target.ll = Utils.findRequiredViewAsType(source, R.id.ll, "field 'll'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Login_Activity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnfacebook = null;
    target.btnlogin = null;
    target.btngoogle = null;
    target.ivloginlogo = null;
    target.evUsername = null;
    target.textInputLayoutUsername = null;
    target.evPassword = null;
    target.textInputLayoutPassword = null;
    target.evPhone = null;
    target.textInputLayoutPhone = null;
    target.v1Line = null;
    target.tvLine = null;
    target.tv = null;
    target.checkBoxRememberMe = null;
    target.v2Line = null;
    target.ll = null;
  }
}
