// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(MainActivity target, View source) {
    this.target = target;

    target.ivHomelogo = Utils.findRequiredViewAsType(source, R.id.iv_Homelogo, "field 'ivHomelogo'", ImageView.class);
    target.tvUsername = Utils.findRequiredViewAsType(source, R.id.tv_Username, "field 'tvUsername'", TextView.class);
    target.btnSignup = Utils.findRequiredViewAsType(source, R.id.btn_Signup, "field 'btnSignup'", Button.class);
    target.btnHomeLogin = Utils.findRequiredViewAsType(source, R.id.btn_Home_Login, "field 'btnHomeLogin'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivHomelogo = null;
    target.tvUsername = null;
    target.btnSignup = null;
    target.btnHomeLogin = null;
  }
}
