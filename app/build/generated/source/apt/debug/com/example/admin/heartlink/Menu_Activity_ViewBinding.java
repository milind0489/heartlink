// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Menu_Activity_ViewBinding implements Unbinder {
  private Menu_Activity target;

  @UiThread
  public Menu_Activity_ViewBinding(Menu_Activity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Menu_Activity_ViewBinding(Menu_Activity target, View source) {
    this.target = target;

    target.btnpreviousManu = Utils.findRequiredViewAsType(source, R.id.btnprevious_manu, "field 'btnpreviousManu'", ImageButton.class);
    target.tvtitleManu = Utils.findRequiredViewAsType(source, R.id.tvtitle_manu, "field 'tvtitleManu'", TextView.class);
    target.btnforwordManu = Utils.findRequiredViewAsType(source, R.id.btnforword_manu, "field 'btnforwordManu'", ImageButton.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.btnimgtxtManu = Utils.findRequiredViewAsType(source, R.id.btnimgtxt_manu, "field 'btnimgtxtManu'", Button.class);
    target.llManu = Utils.findRequiredViewAsType(source, R.id.ll_manu, "field 'llManu'", LinearLayout.class);
    target.btnimgtxtscrollManu = Utils.findRequiredViewAsType(source, R.id.btnimgtxtscroll_manu, "field 'btnimgtxtscrollManu'", Button.class);
    target.btnVerHorz = Utils.findRequiredViewAsType(source, R.id.btn_ver_horz, "field 'btnVerHorz'", Button.class);
    target.btnVertical = Utils.findRequiredViewAsType(source, R.id.btn_vertical, "field 'btnVertical'", Button.class);
    target.btnHorizontel = Utils.findRequiredViewAsType(source, R.id.btn_horizontel, "field 'btnHorizontel'", Button.class);
    target.btnFragment = Utils.findRequiredViewAsType(source, R.id.btn_Fragment, "field 'btnFragment'", Button.class);
    target.btnBottomNavigation = Utils.findRequiredViewAsType(source, R.id.btn_BottomNavigation, "field 'btnBottomNavigation'", Button.class);
    target.btnNavigationView = Utils.findRequiredViewAsType(source, R.id.btn_NavigationView, "field 'btnNavigationView'", Button.class);
    target.btnJsonObject = Utils.findRequiredViewAsType(source, R.id.btn_JsonObject, "field 'btnJsonObject'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Menu_Activity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnpreviousManu = null;
    target.tvtitleManu = null;
    target.btnforwordManu = null;
    target.toolbar = null;
    target.btnimgtxtManu = null;
    target.llManu = null;
    target.btnimgtxtscrollManu = null;
    target.btnVerHorz = null;
    target.btnVertical = null;
    target.btnHorizontel = null;
    target.btnFragment = null;
    target.btnBottomNavigation = null;
    target.btnNavigationView = null;
    target.btnJsonObject = null;
  }
}
