// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BottomNavigationActivity_ViewBinding implements Unbinder {
  private BottomNavigationActivity target;

  @UiThread
  public BottomNavigationActivity_ViewBinding(BottomNavigationActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BottomNavigationActivity_ViewBinding(BottomNavigationActivity target, View source) {
    this.target = target;

    target.tvtitleManu = Utils.findRequiredViewAsType(source, R.id.tvtitle_manu, "field 'tvtitleManu'", TextView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.bottomNavigationView = Utils.findRequiredViewAsType(source, R.id.bottom_navigation_view, "field 'bottomNavigationView'", BottomNavigationView.class);
    target.Bottom_FrameLayout = Utils.findRequiredViewAsType(source, R.id.Bottom_FrameLayout, "field 'Bottom_FrameLayout'", FrameLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BottomNavigationActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvtitleManu = null;
    target.toolbar = null;
    target.bottomNavigationView = null;
    target.Bottom_FrameLayout = null;
  }
}
