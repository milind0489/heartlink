// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SecondActivity_ViewBinding implements Unbinder {
  private SecondActivity target;

  @UiThread
  public SecondActivity_ViewBinding(SecondActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SecondActivity_ViewBinding(SecondActivity target, View source) {
    this.target = target;

    target.ivSecondactivity = Utils.findRequiredViewAsType(source, R.id.iv_secondactivity, "field 'ivSecondactivity'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SecondActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivSecondactivity = null;
  }
}
