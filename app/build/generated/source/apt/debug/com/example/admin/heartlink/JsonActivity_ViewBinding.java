// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class JsonActivity_ViewBinding implements Unbinder {
  private JsonActivity target;

  @UiThread
  public JsonActivity_ViewBinding(JsonActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public JsonActivity_ViewBinding(JsonActivity target, View source) {
    this.target = target;

    target.tvtitleManu = Utils.findRequiredViewAsType(source, R.id.tvtitle_manu, "field 'tvtitleManu'", TextView.class);
    target.navToolbar = Utils.findRequiredViewAsType(source, R.id.nav_toolbar, "field 'navToolbar'", Toolbar.class);
    target.jsonRecyclerView = Utils.findRequiredViewAsType(source, R.id.json_RecyclerView, "field 'jsonRecyclerView'", RecyclerView.class);
    target.tvsimple = Utils.findRequiredViewAsType(source, R.id.tvsimple, "field 'tvsimple'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    JsonActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvtitleManu = null;
    target.navToolbar = null;
    target.jsonRecyclerView = null;
    target.tvsimple = null;
  }
}
