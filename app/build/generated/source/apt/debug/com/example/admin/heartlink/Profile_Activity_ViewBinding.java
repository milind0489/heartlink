// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Profile_Activity_ViewBinding implements Unbinder {
  private Profile_Activity target;

  @UiThread
  public Profile_Activity_ViewBinding(Profile_Activity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Profile_Activity_ViewBinding(Profile_Activity target, View source) {
    this.target = target;

    target.rlsubProfileSetting = Utils.findRequiredViewAsType(source, R.id.rlsub_profile_setting, "field 'rlsubProfileSetting'", Button.class);
    target.rlsubProfile = Utils.findRequiredViewAsType(source, R.id.rlsub_profile, "field 'rlsubProfile'", RelativeLayout.class);
    target.rlsubProfileUseriv = Utils.findRequiredViewAsType(source, R.id.rlsub_profile_useriv, "field 'rlsubProfileUseriv'", CircleImageView.class);
    target.rlsubProfileUserimage = Utils.findRequiredViewAsType(source, R.id.rlsub_profile_userimage, "field 'rlsubProfileUserimage'", RelativeLayout.class);
    target.rlsubProfileDetailTvName = Utils.findRequiredViewAsType(source, R.id.rlsub_profile_detail_tvName, "field 'rlsubProfileDetailTvName'", TextView.class);
    target.rlsubProfileDetailTvAddress = Utils.findRequiredViewAsType(source, R.id.rlsub_profile_detail_tvAddress, "field 'rlsubProfileDetailTvAddress'", TextView.class);
    target.rlsubProfileDetailTvPhoneNumber = Utils.findRequiredViewAsType(source, R.id.rlsub_profile_detail_tvPhoneNumber, "field 'rlsubProfileDetailTvPhoneNumber'", TextView.class);
    target.rlsubProfileDetail = Utils.findRequiredViewAsType(source, R.id.rlsub_profile_detail, "field 'rlsubProfileDetail'", RelativeLayout.class);
    target.rlProfile = Utils.findRequiredViewAsType(source, R.id.rl_profile, "field 'rlProfile'", RelativeLayout.class);
    target.btnclear = Utils.findRequiredViewAsType(source, R.id.clear, "field 'btnclear'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Profile_Activity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rlsubProfileSetting = null;
    target.rlsubProfile = null;
    target.rlsubProfileUseriv = null;
    target.rlsubProfileUserimage = null;
    target.rlsubProfileDetailTvName = null;
    target.rlsubProfileDetailTvAddress = null;
    target.rlsubProfileDetailTvPhoneNumber = null;
    target.rlsubProfileDetail = null;
    target.rlProfile = null;
    target.btnclear = null;
  }
}
