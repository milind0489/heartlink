// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SignupActivity_ViewBinding implements Unbinder {
  private SignupActivity target;

  @UiThread
  public SignupActivity_ViewBinding(SignupActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SignupActivity_ViewBinding(SignupActivity target, View source) {
    this.target = target;

    target.tvtitleSignup = Utils.findRequiredViewAsType(source, R.id.tvtitle_signup, "field 'tvtitleSignup'", TextView.class);
    target.vlineSignup = Utils.findRequiredView(source, R.id.vline_signup, "field 'vlineSignup'");
    target.evfirstnameSignup = Utils.findRequiredViewAsType(source, R.id.evfirstname_signup, "field 'evfirstnameSignup'", EditText.class);
    target.textInputLayoutFirstnamesignup = Utils.findRequiredViewAsType(source, R.id.text_input_layout_firstnamesignup, "field 'textInputLayoutFirstnamesignup'", TextInputLayout.class);
    target.evlastnameSignup = Utils.findRequiredViewAsType(source, R.id.evlastname_signup, "field 'evlastnameSignup'", EditText.class);
    target.textInputLayoutLastnamesignup = Utils.findRequiredViewAsType(source, R.id.text_input_layout_lastnamesignup, "field 'textInputLayoutLastnamesignup'", TextInputLayout.class);
    target.evemailSignup = Utils.findRequiredViewAsType(source, R.id.evemail_signup, "field 'evemailSignup'", EditText.class);
    target.textInputLayoutEmailsignup = Utils.findRequiredViewAsType(source, R.id.text_input_layout_emailsignup, "field 'textInputLayoutEmailsignup'", TextInputLayout.class);
    target.evphoneSignup = Utils.findRequiredViewAsType(source, R.id.evphone_signup, "field 'evphoneSignup'", EditText.class);
    target.textInputLayoutPhsignup = Utils.findRequiredViewAsType(source, R.id.text_input_layout_phsignup, "field 'textInputLayoutPhsignup'", TextInputLayout.class);
    target.evpwdSignup = Utils.findRequiredViewAsType(source, R.id.evpwd_signup, "field 'evpwdSignup'", EditText.class);
    target.textInputLayoutPwdsignup = Utils.findRequiredViewAsType(source, R.id.text_input_layout_pwdsignup, "field 'textInputLayoutPwdsignup'", TextInputLayout.class);
    target.evconfpwdSignup = Utils.findRequiredViewAsType(source, R.id.evconfpwd_signup, "field 'evconfpwdSignup'", EditText.class);
    target.textInputLayoutConfpwdsignup = Utils.findRequiredViewAsType(source, R.id.text_input_layout_confpwdsignup, "field 'textInputLayoutConfpwdsignup'", TextInputLayout.class);
    target.btnSignup = Utils.findRequiredViewAsType(source, R.id.btn_Signup, "field 'btnSignup'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SignupActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvtitleSignup = null;
    target.vlineSignup = null;
    target.evfirstnameSignup = null;
    target.textInputLayoutFirstnamesignup = null;
    target.evlastnameSignup = null;
    target.textInputLayoutLastnamesignup = null;
    target.evemailSignup = null;
    target.textInputLayoutEmailsignup = null;
    target.evphoneSignup = null;
    target.textInputLayoutPhsignup = null;
    target.evpwdSignup = null;
    target.textInputLayoutPwdsignup = null;
    target.evconfpwdSignup = null;
    target.textInputLayoutConfpwdsignup = null;
    target.btnSignup = null;
  }
}
