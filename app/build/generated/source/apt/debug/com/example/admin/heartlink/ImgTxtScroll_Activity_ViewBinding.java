// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ImgTxtScroll_Activity_ViewBinding implements Unbinder {
  private ImgTxtScroll_Activity target;

  @UiThread
  public ImgTxtScroll_Activity_ViewBinding(ImgTxtScroll_Activity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ImgTxtScroll_Activity_ViewBinding(ImgTxtScroll_Activity target, View source) {
    this.target = target;

    target.tvtoolbarTitle = Utils.findRequiredViewAsType(source, R.id.tvtoolbar_title, "field 'tvtoolbarTitle'", TextView.class);
    target.toolbarRecycleview = Utils.findRequiredViewAsType(source, R.id.toolbar_recycleview, "field 'toolbarRecycleview'", Toolbar.class);
    target.toolRecycleview = Utils.findRequiredViewAsType(source, R.id.tool_recycleview, "field 'toolRecycleview'", RecyclerView.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progressBar, "field 'progressBar'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ImgTxtScroll_Activity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvtoolbarTitle = null;
    target.toolbarRecycleview = null;
    target.toolRecycleview = null;
    target.progressBar = null;
  }
}
