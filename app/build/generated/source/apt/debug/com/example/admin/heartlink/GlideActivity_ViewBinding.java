// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GlideActivity_ViewBinding implements Unbinder {
  private GlideActivity target;

  @UiThread
  public GlideActivity_ViewBinding(GlideActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public GlideActivity_ViewBinding(GlideActivity target, View source) {
    this.target = target;

    target.tvtitleManu = Utils.findRequiredViewAsType(source, R.id.tvtitle_manu, "field 'tvtitleManu'", TextView.class);
    target.navToolbar = Utils.findRequiredViewAsType(source, R.id.nav_toolbar, "field 'navToolbar'", Toolbar.class);
    target.ivGlide = Utils.findRequiredViewAsType(source, R.id.iv_glide, "field 'ivGlide'", ImageView.class);
    target.iv2dGlide = Utils.findRequiredViewAsType(source, R.id.iv2d_glide, "field 'iv2dGlide'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    GlideActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvtitleManu = null;
    target.navToolbar = null;
    target.ivGlide = null;
    target.iv2dGlide = null;
  }
}
