// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ver_horz_Activity_ViewBinding implements Unbinder {
  private ver_horz_Activity target;

  @UiThread
  public ver_horz_Activity_ViewBinding(ver_horz_Activity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ver_horz_Activity_ViewBinding(ver_horz_Activity target, View source) {
    this.target = target;

    target.tvtoolbarTitleVerHorz = Utils.findRequiredViewAsType(source, R.id.tvtoolbar_title_ver_horz, "field 'tvtoolbarTitleVerHorz'", TextView.class);
    target.toolbarVerHorz = Utils.findRequiredViewAsType(source, R.id.toolbar_ver_horz, "field 'toolbarVerHorz'", Toolbar.class);
    target.rvHorizontalVerHorz = Utils.findRequiredViewAsType(source, R.id.rv_horizontal_ver_horz, "field 'rvHorizontalVerHorz'", RecyclerView.class);
    target.v22Line = Utils.findRequiredView(source, R.id.v22_line, "field 'v22Line'");
    target.rvVerticalVerHorz = Utils.findRequiredViewAsType(source, R.id.rv_vertical_ver_horz, "field 'rvVerticalVerHorz'", RecyclerView.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progressBar, "field 'progressBar'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ver_horz_Activity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvtoolbarTitleVerHorz = null;
    target.toolbarVerHorz = null;
    target.rvHorizontalVerHorz = null;
    target.v22Line = null;
    target.rvVerticalVerHorz = null;
    target.progressBar = null;
  }
}
