// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Navigation_Activity_ViewBinding implements Unbinder {
  private Navigation_Activity target;

  private View view2131296424;

  @UiThread
  public Navigation_Activity_ViewBinding(Navigation_Activity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Navigation_Activity_ViewBinding(final Navigation_Activity target, View source) {
    this.target = target;

    View view;
    target.tvtitleManu = Utils.findRequiredViewAsType(source, R.id.tvtitle_manu, "field 'tvtitleManu'", TextView.class);
    target.navToolbar = Utils.findRequiredViewAsType(source, R.id.nav_toolbar, "field 'navToolbar'", Toolbar.class);
    target.navView = Utils.findRequiredViewAsType(source, R.id.nav_view, "field 'navView'", NavigationView.class);
    view = Utils.findRequiredView(source, R.id.nav_flotbutton, "field 'navFlotbutton' and method 'onViewClicked'");
    target.navFlotbutton = Utils.castView(view, R.id.nav_flotbutton, "field 'navFlotbutton'", FloatingActionButton.class);
    view2131296424 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.drawerLayout = Utils.findRequiredViewAsType(source, R.id.drawer_layout, "field 'drawerLayout'", DrawerLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Navigation_Activity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvtitleManu = null;
    target.navToolbar = null;
    target.navView = null;
    target.navFlotbutton = null;
    target.drawerLayout = null;

    view2131296424.setOnClickListener(null);
    view2131296424 = null;
  }
}
