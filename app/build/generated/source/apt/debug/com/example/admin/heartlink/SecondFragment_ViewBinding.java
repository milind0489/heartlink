// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SecondFragment_ViewBinding implements Unbinder {
  private SecondFragment target;

  private View view2131296307;

  @UiThread
  public SecondFragment_ViewBinding(final SecondFragment target, View source) {
    this.target = target;

    View view;
    target.tvFregmentSecond = Utils.findRequiredViewAsType(source, R.id.tv_fregment_second, "field 'tvFregmentSecond'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btn_fregment_second, "field 'btnFregmentSecond' and method 'onViewClicked'");
    target.btnFregmentSecond = Utils.castView(view, R.id.btn_fregment_second, "field 'btnFregmentSecond'", Button.class);
    view2131296307 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SecondFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvFregmentSecond = null;
    target.btnFregmentSecond = null;

    view2131296307.setOnClickListener(null);
    view2131296307 = null;
  }
}
