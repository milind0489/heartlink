// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FirstFragment_ViewBinding implements Unbinder {
  private FirstFragment target;

  private View view2131296306;

  @UiThread
  public FirstFragment_ViewBinding(final FirstFragment target, View source) {
    this.target = target;

    View view;
    target.tvFregmentFirst = Utils.findRequiredViewAsType(source, R.id.tv_fregment_first, "field 'tvFregmentFirst'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btn_fregment_first, "field 'btnFregmentFirst' and method 'onViewClicked'");
    target.btnFregmentFirst = Utils.castView(view, R.id.btn_fregment_first, "field 'btnFregmentFirst'", Button.class);
    view2131296306 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    FirstFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvFregmentFirst = null;
    target.btnFregmentFirst = null;

    view2131296306.setOnClickListener(null);
    view2131296306 = null;
  }
}
