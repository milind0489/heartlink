// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GridActivity_ViewBinding implements Unbinder {
  private GridActivity target;

  @UiThread
  public GridActivity_ViewBinding(GridActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public GridActivity_ViewBinding(GridActivity target, View source) {
    this.target = target;

    target.tvtoolbarTitle = Utils.findRequiredViewAsType(source, R.id.tvtoolbar_title, "field 'tvtoolbarTitle'", TextView.class);
    target.toolbarGridview = Utils.findRequiredViewAsType(source, R.id.toolbar_gridview, "field 'toolbarGridview'", Toolbar.class);
    target.rvGrideview = Utils.findRequiredViewAsType(source, R.id.rv_grideview, "field 'rvGrideview'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    GridActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvtoolbarTitle = null;
    target.toolbarGridview = null;
    target.rvGrideview = null;
  }
}
