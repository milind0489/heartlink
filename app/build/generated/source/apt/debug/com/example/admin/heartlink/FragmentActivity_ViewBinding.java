// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentActivity_ViewBinding implements Unbinder {
  private FragmentActivity target;

  @UiThread
  public FragmentActivity_ViewBinding(FragmentActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FragmentActivity_ViewBinding(FragmentActivity target, View source) {
    this.target = target;

    target.btnFirstFragment = Utils.findRequiredViewAsType(source, R.id.btn_firstFragment, "field 'btnFirstFragment'", Button.class);
    target.btnLastfragment = Utils.findRequiredViewAsType(source, R.id.btn_lastfragment, "field 'btnLastfragment'", Button.class);
    target.FrameLayout = Utils.findRequiredViewAsType(source, R.id.FrameLayout, "field 'FrameLayout'", FrameLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FragmentActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnFirstFragment = null;
    target.btnLastfragment = null;
    target.FrameLayout = null;
  }
}
