// Generated code from Butter Knife. Do not modify!
package com.example.admin.heartlink;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditProfileActivity_ViewBinding implements Unbinder {
  private EditProfileActivity target;

  @UiThread
  public EditProfileActivity_ViewBinding(EditProfileActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public EditProfileActivity_ViewBinding(EditProfileActivity target, View source) {
    this.target = target;

    target.tvEditprofile = Utils.findRequiredViewAsType(source, R.id.tv_editprofile, "field 'tvEditprofile'", TextView.class);
    target.viewEditprofile = Utils.findRequiredView(source, R.id.view_editprofile, "field 'viewEditprofile'");
    target.ivimageEditprofile = Utils.findRequiredViewAsType(source, R.id.ivimage_editprofile, "field 'ivimageEditprofile'", ImageView.class);
    target.evnameEditprofile = Utils.findRequiredViewAsType(source, R.id.evname_editprofile, "field 'evnameEditprofile'", EditText.class);
    target.textInputLayoutUser = Utils.findRequiredViewAsType(source, R.id.text_input_layout_user, "field 'textInputLayoutUser'", TextInputLayout.class);
    target.evaddressEditprofile = Utils.findRequiredViewAsType(source, R.id.evaddress_editprofile, "field 'evaddressEditprofile'", EditText.class);
    target.textInputLayoutAddress = Utils.findRequiredViewAsType(source, R.id.text_input_layout_address, "field 'textInputLayoutAddress'", TextInputLayout.class);
    target.evphoneEditprofile = Utils.findRequiredViewAsType(source, R.id.evphone_editprofile, "field 'evphoneEditprofile'", EditText.class);
    target.textInputLayoutPh = Utils.findRequiredViewAsType(source, R.id.text_input_layout_ph, "field 'textInputLayoutPh'", TextInputLayout.class);
    target.btnokEditprofile = Utils.findRequiredViewAsType(source, R.id.btnok_editprofile, "field 'btnokEditprofile'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EditProfileActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvEditprofile = null;
    target.viewEditprofile = null;
    target.ivimageEditprofile = null;
    target.evnameEditprofile = null;
    target.textInputLayoutUser = null;
    target.evaddressEditprofile = null;
    target.textInputLayoutAddress = null;
    target.evphoneEditprofile = null;
    target.textInputLayoutPh = null;
    target.btnokEditprofile = null;
  }
}
