package com.example.admin.heartlink;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Binder;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.IOException;

public class testActivity extends AppCompatActivity {
    private static final int SELECT_PICTURE = 100;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_test );

        ImageView imageView=findViewById( R.id.imageView );
        FloatingActionButton fabtn=findViewById( R.id.btnSelectImage );
    fabtn.setOnClickListener( new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            openImageChooser();

        }


    } );
    }
    private void openImageChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }
    protected void onActicityResult(int requestCode,int resultCode,Intent data) {
        super.onActivityResult( requestCode, resultCode, data );

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectImageUri = data.getData();
                if (null != selectImageUri) {
                    String path = getPathFromUri( selectImageUri );
                   /* log.i( TAG, "Image Path:", path );*/
                    ((ImageView) findViewById( R.id.ivimage_editprofile )).setImageURI( selectImageUri );
                }
            }
        }
    }

    private String getPathFromUri(Uri contentUri) {

            String res = null;
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query( contentUri, proj, null, null, null );

            if (cursor.moveToFirst()) {
                int colum_index = cursor.getColumnIndexOrThrow( MediaStore.Images.Media.DATA );
                res = cursor.getString( colum_index );
            }
            cursor.close();
            return res;
        };

    }

