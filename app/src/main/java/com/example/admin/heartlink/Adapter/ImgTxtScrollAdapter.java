package com.example.admin.heartlink.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.heartlink.Method.ImgTxt;

import java.util.ArrayList;

import com.example.admin.heartlink.R;
import com.example.admin.heartlink.RecyclerView_OnClickListener.OnClickListner;

public class ImgTxtScrollAdapter extends RecyclerView.Adapter <ImgTxtScrollAdapter.UserViewHolder>{

    private ArrayList<ImgTxt> arrayList;
    private Context context;

    public ImgTxtScrollAdapter(Context context, ArrayList<ImgTxt> arrayList){

        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }


    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {


        final ImgTxt model=arrayList.get( position );
        Bitmap image = BitmapFactory.decodeResource( context.getResources(),model.getImage() );
        holder.image.setImageBitmap( image );
        holder.title.setText( model.getImgtitle() );
        holder.subtitle.setText( model.getImgname() );
        holder.setOnClickListner( new OnClickListner()  {
            @Override
            public void OnItemClick(View view, int position) {
                switch (view.getId()){
                    case R.id.liner_layout:
                        Toast.makeText( context, "You have clicked" + model.getImgname()+model.getImgtitle(), Toast.LENGTH_SHORT ).show();
                        break;
                }
            }
        });


    }


    public class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView image;
        public TextView title, subtitle;
        public LinearLayout listlayout;

        private OnClickListner onClickListner;

        public UserViewHolder(View itemView) {
            super( itemView );

            image = itemView.findViewById( R.id.img_txt_list_iv );
            title = itemView.findViewById( R.id.img_txt_list_tv1 );
            subtitle = itemView.findViewById( R.id.img_txt_list_tv2 );
            listlayout = itemView.findViewById( R.id.liner_layout );
            listlayout.setOnClickListener( this );
        }

        @Override
        public void onClick(View v) {

            if (onClickListner != null) {
                onClickListner.OnItemClick( v, getAdapterPosition( ) );
            }
        }
            public void setOnClickListner(OnClickListner onClickListner){
                this.onClickListner = onClickListner;
            }



    }

    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater mInflater = LayoutInflater.from( parent.getContext() );
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate( R.layout.img_txt_list, parent, false );
        UserViewHolder listHolder = new UserViewHolder( mainGroup );
        return listHolder;

    }

}





