package com.example.admin.heartlink;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.heartlink.Adapter.HorizontalAdapter;
import com.example.admin.heartlink.Adapter.ImgTxtAdepter;
import com.example.admin.heartlink.Adapter.ImgTxtScrollAdapter;
import com.example.admin.heartlink.ClickListner.RandomNumberGenerator;
import com.example.admin.heartlink.Method.ImgTxt;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ver_horz_Activity extends AppCompatActivity {

    @BindView(R.id.tvtoolbar_title_ver_horz)
    TextView tvtoolbarTitleVerHorz;
    @BindView(R.id.toolbar_ver_horz)
    Toolbar toolbarVerHorz;
    @BindView(R.id.rv_horizontal_ver_horz)
    RecyclerView rvHorizontalVerHorz;
    @BindView(R.id.v22_line)
    View v22Line;
    @BindView(R.id.rv_vertical_ver_horz)
    RecyclerView rvVerticalVerHorz;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private ImgTxtAdepter mAdapter;                                                             //Adapter for Linearlist recycle view
    private HorizontalAdapter horizontalAdapter;                                          //Adapter for horizontal recycle view
    private List<ImgTxt> userList = new ArrayList<>();
    private List<ImgTxt> horizontalModels = new ArrayList<>();





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.ver_horz_activity );
        ButterKnife.bind( this );
        mAdapter=new ImgTxtAdepter( userList );
        horizontalAdapter=new HorizontalAdapter(horizontalModels);

        RecyclerView.LayoutManager mLayoutManager=new LinearLayoutManager( getApplicationContext() );
        rvVerticalVerHorz.setLayoutManager( mLayoutManager );
        rvVerticalVerHorz.setItemAnimator( new DefaultItemAnimator() );
        rvVerticalVerHorz.setAdapter( mAdapter );

        prepareUserList();

        RecyclerView.LayoutManager hLayoutManager=new LinearLayoutManager( this, LinearLayoutManager.HORIZONTAL,false );
        rvHorizontalVerHorz.setLayoutManager( hLayoutManager );
        rvHorizontalVerHorz.setItemAnimator( new DefaultItemAnimator() );
        rvHorizontalVerHorz.setAdapter( horizontalAdapter );

        prepareHorizontalList();

    }

    private void prepareHorizontalList() {

        int[] img = new int[]{R.mipmap.img5, R.mipmap.img4};

        ImgTxt imgData = new ImgTxt( img[0] ,"milind");
        horizontalModels.add( imgData );

        imgData = new ImgTxt( img[1] ,"milind");
        horizontalModels.add( imgData );

        imgData = new ImgTxt( img[0],"milind" );
        horizontalModels.add( imgData );

        imgData = new ImgTxt( img[1],"milind" );
        horizontalModels.add( imgData );

        imgData = new ImgTxt( img[0] ,"milind");
        horizontalModels.add( imgData );

        imgData = new ImgTxt( img[1] ,"milind");
        horizontalModels.add( imgData );

        imgData = new ImgTxt( img[0],"milind" );
        horizontalModels.add( imgData );

        imgData = new ImgTxt( img[1] ,"milind");
        horizontalModels.add( imgData );

        imgData = new ImgTxt( img[0],"milind" );
        horizontalModels.add( imgData );

        imgData = new ImgTxt( img[1] ,"milind");
        horizontalModels.add( imgData );

        imgData = new ImgTxt( img[0] ,"milind");
        horizontalModels.add( imgData );

        horizontalAdapter.notifyDataSetChanged();
    }

    private void prepareUserList() {
        int[] images = new int[]{R.mipmap.img1, R.mipmap.img2, R.mipmap.img3, R.mipmap.img4, R.mipmap.img5};

        ImgTxt data = new ImgTxt( images[0], "Hello", "Sample text" );
        userList.add( data );

        data = new ImgTxt( images[1], "Hello 2", "Sample text 2" );
        userList.add( data );

        data = new ImgTxt( images[2], "Hello 3", "Sample text 3" );
        userList.add( data );

        data = new ImgTxt( images[3], "Hello 4", "Sample text 4" );
        userList.add( data );

        data = new ImgTxt( images[4], "Hello 5", "Sample text 5" );
        userList.add( data );

        data = new ImgTxt( images[0], "Hello 6", "Sample text 6" );
        userList.add( data );

        data = new ImgTxt( images[1], "Hello 7", "Sample text 7" );
        userList.add( data );

        data = new ImgTxt( images[2], "Hello 8", "Sample text 8" );
        userList.add( data );

        data = new ImgTxt( images[3], "Hello 9", "Sample text 9" );
        userList.add( data );

        data = new ImgTxt( images[4], "Hello 10", "Sample text 10" );
        userList.add( data );

        data = new ImgTxt( images[0], "Hello 11", "Sample text 11" );
        userList.add( data );

        data = new ImgTxt( images[1], "Hello 12", "Sample text 12" );
        userList.add( data );

        mAdapter.notifyDataSetChanged();


    }
}
