package com.example.admin.heartlink.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.heartlink.Method.ImgTxt;
import com.example.admin.heartlink.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ImgTxtAdepter extends RecyclerView.Adapter<ImgTxtAdepter.MyViewHolder>{

    private List<ImgTxt> imgTxtList;

    public class MyViewHolder extends RecyclerView.ViewHolder{


        public TextView imgtitle,imgname;
        public CircleImageView image;


        public MyViewHolder(View itemView) {
            super( itemView );

            imgtitle=itemView.findViewById( R.id.img_txt_list_tv1);
            imgname=itemView.findViewById( R.id.img_txt_list_tv2 );
            image=itemView.findViewById( R.id.img_txt_list_iv );


        }


    }
    public ImgTxtAdepter(List<ImgTxt> imgTxtList){
        this.imgTxtList=imgTxtList;
        /*this.context= context;*/
       

    }




    @Override
    public MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {

            View itemView= LayoutInflater.from( parent.getContext() ).inflate( R.layout.img_txt_list,parent,false );
            return new MyViewHolder( itemView );

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        ImgTxt imgTxt = imgTxtList.get(position);
        holder.imgtitle.setText(imgTxt.getImgtitle());
        holder.imgname.setText(imgTxt.getImgname());
        holder.image.setImageResource(imgTxt.getImage());

    }

    @Override
    public int getItemCount() {
        return imgTxtList.size();
    }


}
