package com.example.admin.heartlink;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Navigation_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.tvtitle_manu)
    TextView tvtitleManu;
    @BindView(R.id.nav_toolbar)
    Toolbar navToolbar;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.nav_flotbutton)
    FloatingActionButton navFlotbutton;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen( GravityCompat.START )) {
            drawerLayout.closeDrawer( GravityCompat.START );
        } else {
            super.onBackPressed( );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater( ).inflate( R.menu.main2, menu );
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.navigation_activity );
        ButterKnife.bind( this );
        Toolbar toolbar = findViewById( R.id.nav_toolbar );
        setSupportActionBar( toolbar );


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle( Navigation_Activity.this, drawerLayout, toolbar, R.string.DrewarO, R.string.DrewarC );
        drawerLayout.addDrawerListener( toggle );
        toggle.syncState( );

        NavigationView navigationView = ( NavigationView ) findViewById( R.id.nav_view );
        navigationView.setNavigationItemSelectedListener( Navigation_Activity.this );


    }


    @OnClick(R.id.nav_flotbutton)
    public void onViewClicked(View view) {


        Snackbar.make( view, "Replace with your own action", Snackbar.LENGTH_LONG )
                .setAction( "Action", null ).show( );
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId( );
        if (id == R.id.navigation_allsong) {

        } else if (id == R.id.navigation_genres) {

        } else if (id == R.id.navigation_albums) {

        } else if (id == R.id.navigation_artists) {

        }

        drawerLayout.closeDrawer( GravityCompat.START );
        return true;
    }
}
