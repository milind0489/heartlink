package com.example.admin.heartlink;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

@SuppressLint("ValidFragment")
public class SecondFragment extends Fragment {

    View view;
    Button secoundbutton;
    @BindView(R.id.tv_fregment_second)
    TextView tvFregmentSecond;
    @BindView(R.id.btn_fregment_second)
    Button btnFregmentSecond;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        /*return super.onCreateView( inflater, container, savedInstanceState );*/

        view = inflater.inflate( R.layout.fragment_second, container, false );

        return view;
    }


    @OnClick(R.id.btn_fregment_second)
    public void onViewClicked() {

        Toast.makeText( getActivity(), "Second Fragment", Toast.LENGTH_LONG).show();

    }
}
