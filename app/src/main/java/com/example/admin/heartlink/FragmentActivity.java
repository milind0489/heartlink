package com.example.admin.heartlink;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentActivity extends AppCompatActivity {

    @BindView(R.id.btn_firstFragment)
    Button btnFirstFragment;
    @BindView(R.id.btn_lastfragment)
    Button btnLastfragment;
    @BindView(R.id.FrameLayout)
    android.widget.FrameLayout FrameLayout;
    String Tag="abc";


    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 1){
            finish();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.fragment_activity );
        ButterKnife.bind( this );

        btnFirstFragment.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                loadFragment(new FirstFragment());
                /*Fragment myFrag=new Fragment();
                FragmentManager manager=getFragmentManager();
                FragmentTransaction ft=manager.beginTransaction();
                ft.addToBackStack( null );
                ft.commit();*/
                Log.e( Tag,"text" );

            }
        } );
        btnLastfragment.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                loadFragment(new  SecondFragment());
               /* Fragment myFrag=new Fragment();
                FragmentManager manager=getFragmentManager();
                FragmentTransaction ft=manager.beginTransaction();
                ft.addToBackStack( null );
                ft.commit();*/
                Log.e( Tag,"text" );
            }
        } );
    }




    public void loadFragment(Fragment fragment) {
        String backStateName = fragment.getClass( ).getName( );
        String fragmentTag = backStateName;

        FragmentManager fm = getFragmentManager( );
        boolean fragmentPopped = fm.popBackStackImmediate( backStateName, 0 );

        if (!fragmentPopped && fm.findFragmentByTag( fragmentTag ) == null) {

            FragmentTransaction fragmentTransaction = fm.beginTransaction( );
            fragmentTransaction.replace( R.id.FrameLayout, fragment ,fragmentTag);
            fragmentTransaction.addToBackStack( backStateName );

            fragmentTransaction.commit( );
        }

    }

}
