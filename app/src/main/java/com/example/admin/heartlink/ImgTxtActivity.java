package com.example.admin.heartlink;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.admin.heartlink.Adapter.ImgTxtAdepter;
import com.example.admin.heartlink.Method.ImgTxt;

import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ImgTxtActivity extends AppCompatActivity {

    @BindView(R.id.tvtoolbar_title)
    TextView tvtoolbarTitle;
    @BindView(R.id.toolbar_recycleview)
    Toolbar toolbarRecycleview;
    @BindView(R.id.tool_recycleview)
    RecyclerView tool_recycleview;

    public List<ImgTxt> imgTxtList = new ArrayList<>();
    public ImgTxtAdepter imgTxtAdepter;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.img_txt_activity );
        ButterKnife.bind( this );
        setSupportActionBar( toolbarRecycleview );

        imgTxtAdepter = new ImgTxtAdepter( imgTxtList );
        RecyclerView.LayoutManager mlayoutManager = new LinearLayoutManager( getApplicationContext() );
        tool_recycleview.setLayoutManager( mlayoutManager );
        tool_recycleview.setItemAnimator( new DefaultItemAnimator() );
        tool_recycleview.setAdapter( imgTxtAdepter );



         prepareImgTxtData();


    }




    public void prepareImgTxtData() {
        int[] images = new int[]{R.drawable.setting, R.drawable.homelogored, R.drawable.email};
        ImgTxt imgTxt = new ImgTxt( images[0], "image1", "android" );
        imgTxtList.add( imgTxt ) ;

        imgTxt = new ImgTxt( images[1], "homepage2", "android" );
        imgTxtList.add( imgTxt ) ;

        imgTxt = new ImgTxt( images[2], "facebook3", "android" );
        imgTxtList.add( imgTxt ) ;

        imgTxt = new ImgTxt( images[0], "image4", "android" );
        imgTxtList.add( imgTxt ) ;

        imgTxt = new ImgTxt( images[1], "homepage5", "android" );
        imgTxtList.add( imgTxt ) ;

        imgTxt = new ImgTxt( images[2], "facebook6", "android" );
        imgTxtList.add( imgTxt ) ;
        imgTxt = new ImgTxt( images[0], "image7", "android" );
        imgTxtList.add( imgTxt ) ;

        imgTxt = new ImgTxt( images[1], "homepage8", "android" );
        imgTxtList.add( imgTxt ) ;

        imgTxt = new ImgTxt( images[2], "facebook9", "android" );
        imgTxtList.add( imgTxt ) ;

        imgTxt = new ImgTxt( images[0], "image10", "android" );
        imgTxtList.add( imgTxt ) ;

        imgTxt = new ImgTxt( images[1], "homepage11", "android" );
        imgTxtList.add( imgTxt ) ;

        imgTxt = new ImgTxt( images[2], "facebook12", "android" );
        imgTxtList.add( imgTxt ) ;

        imgTxtAdepter.notifyDataSetChanged();
       /* return new ArrayList();*/
        /*return null;*/
    }
}
