package com.example.admin.heartlink;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditProfileActivity extends AppCompatActivity {

    ImageView imageView;
    @BindView(R.id.tv_editprofile)
    TextView tvEditprofile;
    @BindView(R.id.view_editprofile)
    View viewEditprofile;
    @BindView(R.id.ivimage_editprofile)
    ImageView ivimageEditprofile;
    @BindView(R.id.evname_editprofile)
    EditText evnameEditprofile;
    @BindView(R.id.text_input_layout_user)
    TextInputLayout textInputLayoutUser;
    @BindView(R.id.evaddress_editprofile)
    EditText evaddressEditprofile;
    @BindView(R.id.text_input_layout_address)
    TextInputLayout textInputLayoutAddress;
    @BindView(R.id.evphone_editprofile)
    EditText evphoneEditprofile;
    @BindView(R.id.text_input_layout_ph)
    TextInputLayout textInputLayoutPh;
    @BindView(R.id.btnok_editprofile)
    Button btnokEditprofile;
    private int PICK_IMAGE_REQUEST = 100;
    private int RESULE_OK = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.edit_profile_activity );
        ButterKnife.bind( this );



        Intent intent = getIntent();
        String ename = getIntent().getStringExtra( "username" );
        evnameEditprofile.setText( ename );
        evnameEditprofile.setTextSize( 25 );
        String ephone = getIntent().getStringExtra( "phonenumber" );
        evphoneEditprofile.setText( ephone );
        evphoneEditprofile.setTextSize( 25 );


        btnokEditprofile.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Toast.makeText( EditProfileActivity.this, "Edit Succeasfull", Toast.LENGTH_LONG ).show();

                Intent i = new Intent( EditProfileActivity.this, Profile_Activity.class );
                String strname = evnameEditprofile.getText().toString();
                i.putExtra( "username", strname );
                String straddress = evaddressEditprofile.getText().toString();
                i.putExtra( "address", straddress );
                String strphone = evphoneEditprofile.getText().toString();
                i.putExtra( "phonenumber", strphone );
                startActivity( i );
            }
        } );

        imageView.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setType( "Image/*" );
                intent.setAction( intent.ACTION_GET_CONTENT );
                startActivityForResult( Intent.createChooser( intent, "Select Picture" ), PICK_IMAGE_REQUEST );

            }
        } );

    }

    protected void onActicityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data );

        if (requestCode == PICK_IMAGE_REQUEST && requestCode == RESULE_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap( getContentResolver(), uri );
                ImageView imageView = (ImageView) findViewById( R.id.ivimage_editprofile );
                imageView.setImageBitmap( bitmap );

            } catch (IOException e) {
                e.printStackTrace();
            }


            String[] projection = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query( uri, projection, null, null, null );
            cursor.moveToFirst();

            /* Log.d( Tag, DatabaseUtils.dumpCursorToString(cursor));*/

            int columnIndex = cursor.getColumnIndex( projection[0] );
            String picturePath = cursor.getString( columnIndex ); // returns null
            cursor.close();

        }

    }

    ;
}
