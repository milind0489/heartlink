package com.example.admin.heartlink;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.heartlink.Utils.MYsharedPreferences;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SignupActivity extends AppCompatActivity {


    @BindView(R.id.tvtitle_signup)
    TextView tvtitleSignup;
    @BindView(R.id.vline_signup)
    View vlineSignup;
    @BindView(R.id.evfirstname_signup)
    EditText evfirstnameSignup;
    @BindView(R.id.text_input_layout_firstnamesignup)
    TextInputLayout textInputLayoutFirstnamesignup;
    @BindView(R.id.evlastname_signup)
    EditText evlastnameSignup;
    @BindView(R.id.text_input_layout_lastnamesignup)
    TextInputLayout textInputLayoutLastnamesignup;
    @BindView(R.id.evemail_signup)
    EditText evemailSignup;
    @BindView(R.id.text_input_layout_emailsignup)
    TextInputLayout textInputLayoutEmailsignup;
    @BindView(R.id.evphone_signup)
    EditText evphoneSignup;
    @BindView(R.id.text_input_layout_phsignup)
    TextInputLayout textInputLayoutPhsignup;
    @BindView(R.id.evpwd_signup)
    EditText evpwdSignup;
    @BindView(R.id.text_input_layout_pwdsignup)
    TextInputLayout textInputLayoutPwdsignup;
    @BindView(R.id.evconfpwd_signup)
    EditText evconfpwdSignup;
    @BindView(R.id.text_input_layout_confpwdsignup)
    TextInputLayout textInputLayoutConfpwdsignup;
    @BindView(R.id.btn_Signup)
    Button btnSignup;

    private String text;

    private MYsharedPreferences sharedPreference;

    Activity context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.signup_activity );
        ButterKnife.bind( this );
        sharedPreference = new MYsharedPreferences();

        boolean invalid = false;

        if (evfirstnameSignup.equals( "" )) {
            invalid = true;
            Toast.makeText( this, "enter first name", Toast.LENGTH_LONG ).show();
        } else if (evlastnameSignup.equals( "" )) {
            invalid = true;
            Toast.makeText( this, "enter first name", Toast.LENGTH_LONG ).show();
        } else if (evpwdSignup.length() < 6) {
            invalid = true;
            Toast.makeText( getApplicationContext(),
                    "Please enter atleast 6 digits password", Toast.LENGTH_SHORT ).show();
        }


        btnSignup.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text=evfirstnameSignup.getText().toString();
                sharedPreference.save( context,text );

                Intent intent = new Intent( SignupActivity.this, Login_Activity.class );

                String name = evfirstnameSignup.getText().toString();
                intent.putExtra( "firstname", name );
                String phoneno = evphoneSignup.getText().toString();
                intent.putExtra( "phonenumber", phoneno );
                String pwd = evpwdSignup.getText().toString();
                intent.putExtra( "password", pwd );

                String e_mail = evemailSignup.getText().toString();
                intent.putExtra( "email", e_mail );
                startActivity( intent );
            }
        } );


    }


}
