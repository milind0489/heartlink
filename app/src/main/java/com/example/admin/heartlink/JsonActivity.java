package com.example.admin.heartlink;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.admin.heartlink.InterFace.hospitallist;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JsonActivity extends AppCompatActivity implements hospitallist {

    @BindView(R.id.tvtitle_manu)
    TextView tvtitleManu;
    @BindView(R.id.nav_toolbar)
    Toolbar navToolbar;
    @BindView(R.id.json_RecyclerView)
    RecyclerView jsonRecyclerView;
    @BindView(R.id.tvsimple)
    TextView tvsimple;

    public static View view;
    public static ArrayList <jsonhospitals> listArrayList = new ArrayList <>( );
    public static JsonrecycleviewAdapter jsonrecycleviewAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.json_activity );
        ButterKnife.bind( this );

        jsonrecycleviewAdapter = new JsonrecycleviewAdapter( listArrayList, ( hospitallist ) this );
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getApplicationContext( ) );
        jsonRecyclerView.setLayoutManager( layoutManager );
        jsonRecyclerView.setItemAnimator( new DefaultItemAnimator( ) );
        jsonRecyclerView.setAdapter( jsonrecycleviewAdapter );

        preparejsonhospitalsData( );
/*
        String text = "{\"result\": {\n" +
                "        \"userId\": \"806\",\n" +
                "        \"name\": \"Joseph Chuwa\",\n" +
                "        \"email\": \"harmistest@gmail.com\",\n" +
                "        \"phone\": \"0788888888\",\n" +
                "        \"username\": \"0788888888\",\n" +
                "        \"userpoint\": 0,\n" +
                "        \"profileImage\": \"http://dli.tanzmed.co.tz/images/avatar/thumb_c6e70c50e459429513ed4b96.jpg\",\n" +
                "        \"badgeCount\": 0,\n" +
                "        \"registerDate\": \"2018-04-11 14:02:50\",\n" +
                "        \"userprogress\": 85,\n" +
                "        \"notiCount\": \"10\",\n" +
                "        \"enableNotification\": \"1\",\n" +
                "        \"isenableReminder\": \"1\",\n" +
                "        \"userLocation\": \"\",\n" +
                "        \"childId\": 9\n" +
                "    }\n}";

        String milind = "{\t\"id\": \"0001\",\n" +
                "\t\"type\": \"donut\",\n" +
                "\t\"name\": \"Cake\",\n" +
                "\t\"image\":\n" +
                "\t\t{\n" +
                "\t\t\t\"url\": \"images\0001.jpg\",\n" +
                "\t\t\t\"width\": 200,\n" +
                "\t\t\t\"height\": 200\n" +
                "\t\t},\n" +
                "\t\"thumbnail\":\n" +
                "\t\t{\n" +
                "\t\t\t\"url\": \"images/thumbnails/0001.jpg\",\n" +
                "\t\t\t\"width\": 32,\n" +
                "\t\t\t\"height\": 32\n" +
                "\t\t}}";

        String milind2 = "{ \"name\":\"John\",\n" +
                "    \"age\":30,\n" +
                "    \"cars\": {\n" +
                "        \"car1\":\"Ford\",\n" +
                "        \"car2\":\"BMW\",\n" +
                "        \"car3\":\"Fiat\"\n" +
                "    }}";
        String josn = "{\"success\":1,\"message\":\"Your password is reset successfully,you may now login with new password.\"}";
        String josnn = "{\"success\": 1,\"catId\": \"12\",\"catPoints\": \"4\"}";
        String josnnn = "{\"success\":0,\"message\":\"No Data Available.\"}";
        try {

            JSONObject jsonObject = new JSONObject( josn );
            Log.e( "test", jsonObject.getString( "message" ) );
            Log.e( "test", jsonObject.toString( ) );

            JSONObject jsonObjectt = new JSONObject( josnn );
            Log.e( "test1", jsonObjectt.toString( ) );
            Log.e( "test", jsonObjectt.getString( "catPoints" ) );

            JSONObject jsonObjecttt = new JSONObject( josnnn );
            Log.e( "text2", jsonObjecttt.toString( ) );
            Log.e( "text2", jsonObjecttt.getString( "success" ) );

            JSONObject object = new JSONObject( text );
            JSONObject resultobj = new JSONObject( object.getString( "result" ) );
            Log.e( "tab", resultobj.getString( "userId" ) );


            JSONObject mainobject = new JSONObject( milind );
            JSONObject image0bject = new JSONObject( mainobject.getString( "image" ) );
            Log.e( "Image", image0bject.getString( "url" ) );
            JSONObject thumbnailObject = new JSONObject( mainobject.getString( "thumbnail" ) );
            Log.e( "thum", thumbnailObject.getString( "width" ) );


            JSONObject aaaobject = new JSONObject( milind2 );
            Log.e( "aaa", aaaobject.toString( ) );
            Log.e( "aaa", aaaobject.getString( "name" ) );
            JSONObject carobject = new JSONObject( aaaobject.getString( "cars" ) );
            Log.e( "carname", carobject.getString( "car3" ) );

            String result = "{ \"Employee\" :  \n" +
                    "    [  \n" +
                    "     {\"id\":\"101\",\"name\":\"Sonoo Jaiswal\",\"salary\":\"50000\"},  \n" +
                    "     {\"id\":\"102\",\"name\":\"Vimal Jaiswal\",\"salary\":\"60000\"}  \n" +
                    "    ]   \n" +
                    "}";

            JSONObject resultArray = new JSONObject( result );
            JSONArray jsonArray = resultArray.getJSONArray( "Employee" );
            for (int i = 0; i <= jsonArray.length( ); i++) {
                JSONObject resultoobj = jsonArray.getJSONObject( i );
                Log.e( "array0", resultoobj.getString( "id" ) );
                Log.e( "array1", resultoobj.getString( "name" ) );
                Log.e( "array2", resultoobj.getString( "salary" ) );
            }

            String resultob = "";

        } catch (JSONException e) {
            e.printStackTrace( );
        }*/
    String s="{\n" +
            "    \"success\": 1,\n" +
            "    \"result\": [\n" +
            "        {\n" +
            "            \"id\": \"706\",\n" +
            "            \"title\": \"TUMAINI POST TEST\",\n" +
            "            \"description\": \"Kuhusu sisi\\r\\nKuandaa mazingira bora na kuwapa ushauri nasaha wanaoishi na HIV\\r\\nTunapatikana UKONGA KMKGM\",\n" +
            "            \"categoryName\": \"Mashirika (NGO)\",\n" +
            "            \"catid\": \"7\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"totalCount\": 1\n" +
            "}\n";

    try {
        JSONObject jsonObject=new JSONObject( s );
        JSONArray jsonArray=jsonObject.getJSONArray(  "result");
        for (int i=0;i<=jsonArray.length();i++){

            JSONObject object=jsonArray.getJSONObject( i );
            Log.e( "array0",object.toString() );
            Log.e( "array1",jsonObject.getString( "success" ) );
            Log.e( "array2",jsonObject.getString( "result" ) );
            Log.e( "array3",jsonObject.getString( "totalCount" ) );
            Log.e("array4",object.getString( "title" ));
        }

    } catch (JSONException e) {
        e.printStackTrace( );
    }

    }

    private void preparejsonhospitalsData() {

        String model = " { \"hospitals\": [\n" +
                "        {\n" +
                "            \"id\": \"1\",\n" +
                "            \"name\": \"Regency Medical Center\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"2\",\n" +
                "            \"name\": \"Aga Khan Hospital\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"3\",\n" +
                "            \"name\": \"Sanitas Hospital\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"8\",\n" +
                "            \"name\": \"TMJ Hospital\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"9\",\n" +
                "            \"name\": \"Amana Hospital\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"10\",\n" +
                "            \"name\": \"BOCHI \"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"11\",\n" +
                "            \"name\": \"Cardinal Rugambwa\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"17\",\n" +
                "            \"name\": \"Ekenywa\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"21\",\n" +
                "            \"name\": \"IMTU \"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"26\",\n" +
                "            \"name\": \"Kibangu Mission\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"45\",\n" +
                "            \"name\": \"Kinondoni \"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"49\",\n" +
                "            \"name\": \"Lugalo\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"54\",\n" +
                "            \"name\": \"Marie Stopes Mwenge\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"56\",\n" +
                "            \"name\": \"Massana\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"62\",\n" +
                "            \"name\": \"Mbagala Rangi Tatu\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"63\",\n" +
                "            \"name\": \"Mbweni Mission\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"65\",\n" +
                "            \"name\": \"Kairuki\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"66\",\n" +
                "            \"name\": \"Mikumi\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"67\",\n" +
                "            \"name\": \"Miracolo\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"68\",\n" +
                "            \"name\": \"Mnazi Mmoja\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"71\",\n" +
                "            \"name\": \"Mount Ukombozi\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"76\",\n" +
                "            \"name\": \"Msasani Peninsula \"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"81\",\n" +
                "            \"name\": \"Muhimbili\"\n" +
                "        }]}";
        try {
            JSONObject jsonObject = new JSONObject( model );
            JSONArray jsonArray = jsonObject.getJSONArray( "hospitals" );
            for (int i = 0; i <= jsonArray.length( ); i++) {
                JSONObject object = jsonArray.getJSONObject( i );
                Log.e( "model", object.getString( "name" ) );
                jsonhospitals jsonhospital = new jsonhospitals( object.getString( "name" ) );
                listArrayList.add( jsonhospital );
            }

        } catch (JSONException e) {
            e.printStackTrace( );
        }
        jsonrecycleviewAdapter.notifyDataSetChanged( );
    }


    @Override
    public void diplay(jsonhospitals jsonhospitals) {
        String data = jsonhospitals.getName( ).toString( );
        tvsimple.setText( data );

    }
}
