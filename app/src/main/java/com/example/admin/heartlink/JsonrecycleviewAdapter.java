package com.example.admin.heartlink;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.heartlink.Adapter.ImgTxtAdepter;
import com.example.admin.heartlink.InterFace.hospitallist;

import java.util.ArrayList;

public class JsonrecycleviewAdapter extends RecyclerView.Adapter<JsonrecycleviewAdapter.MyViewHolder>{

    private ArrayList<jsonhospitals> arrayList1;
    hospitallist hospitall;

    public JsonrecycleviewAdapter(ArrayList <jsonhospitals> arrayList1,  hospitallist hospitall){

        this.arrayList1=arrayList1;
        this.hospitall=hospitall;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from( parent.getContext() ).inflate( R.layout.txt_list,parent,false );
        return new MyViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final jsonhospitals jsonhospital=arrayList1.get( position );
        holder.textView.setText( jsonhospital.getName() );
        holder.textView.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                hospitall.diplay( jsonhospital );
            }
        } );
    }

    @Override
    public int getItemCount() {
        return arrayList1.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textView,simpletv;

        public MyViewHolder(View itemView) {
            super( itemView );
            textView=itemView.findViewById( R.id.tvlist_json_hospitel);

        }
    }
}
