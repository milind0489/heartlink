package com.example.admin.heartlink;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecondActivity extends AppCompatActivity {

    @BindView(R.id.iv_secondactivity)
    ImageView ivSecondactivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.second_activity );
        ButterKnife.bind( this );


        Intent intent  =getIntent();
        ivSecondactivity.setImageResource( intent.getIntExtra( "image" ,0) );

    }
}
