package com.example.admin.heartlink;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.iv_Homelogo)
    ImageView ivHomelogo;
    @BindView(R.id.tv_Username)
    TextView tvUsername;
    @BindView(R.id.btn_Signup)
    Button btnSignup;
    @BindView(R.id.btn_Home_Login)
    Button btnHomeLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        ButterKnife.bind( this );

        btnHomeLogin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentlogin = new Intent( MainActivity.this, Login_Activity.class );
                startActivity( intentlogin );
            }
        } );
        btnSignup.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentsignup = new Intent( MainActivity.this, SignupActivity.class );
                startActivity( intentsignup );
            }
        } );
    }
}
