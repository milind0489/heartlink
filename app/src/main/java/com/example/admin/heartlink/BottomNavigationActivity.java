package com.example.admin.heartlink;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BottomNavigationActivity extends AppCompatActivity {

    @BindView(R.id.tvtitle_manu)
    TextView tvtitleManu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bottom_navigation_view)
    BottomNavigationView bottomNavigationView;
    Animation animation, animationLeft, animationslid, animationzoom;
    @BindView(R.id.Bottom_FrameLayout)
    FrameLayout Bottom_FrameLayout;

    String genres="o",albums="o",artists="o";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.bottom_navigation_activity );
        ButterKnife.bind( this );
        /*final Animation animation,animationLeft,animationslid;*/

        /*loadFragment( new FirstFragment( ) ,Test);*/

        animation = AnimationUtils.loadAnimation( getApplicationContext( ), R.anim.bounce );
        animationLeft = AnimationUtils.loadAnimation( getApplicationContext( ), R.anim.slide_left );
        animationslid = AnimationUtils.loadAnimation( getApplicationContext( ), R.anim.sequential );
        animationzoom = AnimationUtils.loadAnimation( getApplicationContext( ), R.anim.zoom_in );
        bottomNavigationView.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener( ) {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {



                switch (item.getItemId( )) {
                    case R.id.navigation_allsong:
                        tvtitleManu.setText( "All songs" );
                        tvtitleManu.startAnimation( animation );
                        loadFragment( new FirstFragment( ), artists);



                        break;
                    case R.id.navigation_genres:
                        tvtitleManu.setText( "genres" );
                        tvtitleManu.startAnimation( animationLeft );
                        loadFragment( new SecondFragment( ),artists );
                        break;
                    case R.id.navigation_albums:
                        tvtitleManu.setText( "albums" );
                        tvtitleManu.startAnimation( animationslid );
                        loadFragment( new FirstFragment( ) ,albums);
                        break;
                    case R.id.navigation_artists:
                        tvtitleManu.setText( "artists" );
                        tvtitleManu.startAnimation( animationzoom );
                        loadFragment( new SecondFragment( ),artists );
                        break;
                    default:

                        break;

                }
                return true;
            }
        } );
    }



    public void loadFragment(Fragment fragment,String name) {

        final FragmentManager fragmentManager = getFragmentManager( );
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction( );
        fragmentTransaction.replace( R.id.Bottom_FrameLayout, fragment );
        fragmentTransaction.addToBackStack( "artists" );
        fragmentTransaction.addToBackStack( name );
        fragmentTransaction.commit();
        }

}
