package com.example.admin.heartlink;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.example.admin.heartlink.Utils.MYsharedPreferences;
import butterknife.BindView;
import butterknife.ButterKnife;

public class Login_Activity extends AppCompatActivity {

    public static Activity activity = null;
    @BindView(R.id.btn_facebook)
    Button btnfacebook;
    @BindView(R.id.btn_login)
    Button btnlogin;
    @BindView(R.id.btn_google)
    Button btngoogle;
    @BindView(R.id.ivloginlogo)
    ImageView ivloginlogo;
    @BindView(R.id.evUsername)
    EditText evUsername;
    @BindView(R.id.text_input_layout_username)
    TextInputLayout textInputLayoutUsername;
    @BindView(R.id.evPassword)
    EditText evPassword;
    @BindView(R.id.text_input_layout_password)
    TextInputLayout textInputLayoutPassword;
    @BindView(R.id.evPhone)
    EditText evPhone;
    @BindView(R.id.text_input_layout_phone)
    TextInputLayout textInputLayoutPhone;
    @BindView(R.id.v1_line)
    View v1Line;
    @BindView(R.id.tv_line)
    TextView tvLine;
    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.checkBoxRememberMe)
    CheckBox checkBoxRememberMe;
    @BindView(R.id.v2_line)
    View v2Line;
    @BindView(R.id.ll)
    LinearLayout ll;

    private String text;

    private MYsharedPreferences sharedPreference;

    Activity context = this;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.login_activity );
        ButterKnife.bind( this );

        sharedPreference=new MYsharedPreferences();
        text=sharedPreference.getValue(context);
        evUsername.setText( text );


        Intent intent = getIntent();
        /*String fname = getIntent().getStringExtra( "firstname" );
        evUsername.setText( fname );*/
        String ph = getIntent().getStringExtra( "phonenumber" );
        evPhone.setText( ph );
        String pwd = getIntent().getStringExtra( "password" );
        evPassword.setText( pwd );
       /* String address = getIntent().getStringExtra( "email" );
        .setText( address );*/

        btnlogin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (evUsername.getText().toString().equals( "milind" ) &&
                        evPassword.getText().toString().equals( "milind" )) {
                    Toast.makeText( getApplicationContext(),
                            "Redirecting...", Toast.LENGTH_SHORT ).show();

                    AwesomeValidation awesomeValidation = new AwesomeValidation( ValidationStyle.BASIC );
                    awesomeValidation.addValidation( Login_Activity.this, R.id.evUsername, "[a-zA-Z\\s]+", R.string.usererr );
                    awesomeValidation.addValidation( Login_Activity.this, R.id.evPhone, "^[+]?[0-9]{10,13}$", R.string.phonerr );
                    awesomeValidation.addValidation( Login_Activity.this, R.id.evPassword, ".{5,}", R.string.passworderr );
                    awesomeValidation.validate();


                    Intent intent1 = new Intent( Login_Activity.this, Profile_Activity.class );

                    String name = evUsername.getText().toString();
                    intent1.putExtra( "username", name );
                    String phone = evPhone.getText().toString();
                    intent1.putExtra( "phonenumber", phone );
                    String text = tv.getText().toString();
                    intent1.putExtra( "address", text );

                    startActivity( intent1 );

                } else {
                    Toast.makeText( getApplicationContext(), "Wrong Credentials", Toast.LENGTH_SHORT ).show();

                }

            }
        } );

        btnfacebook.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent( Intent.ACTION_VIEW, Uri.parse( "https://www.facebook.com/" ) );
                startActivity( i );
            }
        } );

        btngoogle.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent( Intent.ACTION_VIEW, Uri.parse( "https://www.google.co.in/" ) );
                startActivity( i );

            }
        } );


    }



}




