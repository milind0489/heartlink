package com.example.admin.heartlink;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.admin.heartlink.Adapter.Gridadapter;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GridActivity extends AppCompatActivity {

    @BindView(R.id.tvtoolbar_title)
    TextView tvtoolbarTitle;
    @BindView(R.id.toolbar_gridview)
    Toolbar toolbarGridview;
    @BindView(R.id.rv_grideview)
    RecyclerView rvGrideview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.grid_view_activity );
        ButterKnife.bind( this );
        setSupportActionBar( toolbarGridview);
        ArrayList celebritiesName=new ArrayList<>( Arrays.asList( "celebrities 1","celebrities 2","celebrities 3","celebrities 4","celebrities 5","celebrities 6",
                "celebrities 7","celebrities 8","celebrities 9","celebrities 10","celebrities 11","celebrities 12","celebrities 13",
                "celebrities 14"));
        ArrayList celebritiesImage=new ArrayList<>(Arrays.asList( R.drawable.celebrities1,R.drawable.celebrities2,R.drawable.celebrities3,R.drawable.celebrities4,R.drawable.celebrities5,
                R.drawable.celebrities6,R.drawable.celebrities7,R.drawable.celebrities8,R.drawable.celebrities9,R.drawable.celebrities10,
                R.drawable.celebrities11,R.drawable.celebrities12,R.drawable.celebrities13,R.drawable.celebrities14));

        RecyclerView recyclerView=findViewById( R.id.rv_grideview );
        GridLayoutManager gridLayoutManager=new GridLayoutManager( getApplicationContext() ,3);
        recyclerView.setLayoutManager( gridLayoutManager );
        Gridadapter gridadapter=new Gridadapter(GridActivity.this,celebritiesImage,celebritiesName);
        recyclerView.setAdapter( gridadapter );

    }
}
