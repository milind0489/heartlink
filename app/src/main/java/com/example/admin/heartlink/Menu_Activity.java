package com.example.admin.heartlink;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Menu_Activity extends AppCompatActivity {

    @BindView(R.id.btnprevious_manu)
    ImageButton btnpreviousManu;
    @BindView(R.id.tvtitle_manu)
    TextView tvtitleManu;
    @BindView(R.id.btnforword_manu)
    ImageButton btnforwordManu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnimgtxt_manu)
    Button btnimgtxtManu;
    @BindView(R.id.ll_manu)
    LinearLayout llManu;
    @BindView(R.id.btnimgtxtscroll_manu)
    Button btnimgtxtscrollManu;
    @BindView(R.id.btn_ver_horz)
    Button btnVerHorz;
    @BindView(R.id.btn_vertical)
    Button btnVertical;
    @BindView(R.id.btn_horizontel)
    Button btnHorizontel;
    @BindView(R.id.btn_Fragment)
    Button btnFragment;
    @BindView(R.id.btn_BottomNavigation)
    Button btnBottomNavigation;
    @BindView(R.id.btn_NavigationView)
    Button btnNavigationView;
    @BindView(R.id.btn_JsonObject)
    Button btnJsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.menu_activity );
        ButterKnife.bind( this );


        setSupportActionBar( toolbar );

        btnimgtxtManu.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Menu_Activity.this, ImgTxtActivity.class );
                startActivity( intent );
            }
        } );
        btnimgtxtscrollManu.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Menu_Activity.this, ImgTxtScroll_Activity.class );
                startActivity( intent );

            }
        } );
        btnVerHorz.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Menu_Activity.this, ver_horz_Activity.class );
                startActivity( intent );

            }
        } );
        btnVertical.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Menu_Activity.this, GridActivity.class );
                startActivity( intent );

            }
        } );
        btnHorizontel.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Menu_Activity.this, GridActivity.class );
                startActivity( intent );
            }
        } );
        btnFragment.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Menu_Activity.this, FragmentActivity.class );
                startActivity( intent );
            }
        } );
        btnBottomNavigation.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Menu_Activity.this, BottomNavigationActivity.class );
                startActivity( intent );

            }
        } );
        btnNavigationView.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Menu_Activity.this, Navigation_Activity.class );
                startActivity( intent );
            }
        } );
        btnJsonObject.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Menu_Activity.this, JsonActivity.class );
                startActivity( intent );
            }
        } );

    }
}
