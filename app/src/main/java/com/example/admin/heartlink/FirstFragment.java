package com.example.admin.heartlink;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;

@SuppressLint("ValidFragment")
public class FirstFragment extends Fragment {

    View view;

    @BindView(R.id.tv_fregment_first)
    TextView tvFregmentFirst;
    @BindView(R.id.btn_fregment_first)
    Button btnFregmentFirst;
    Unbinder unbinder;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        /*return super.onCreateView( inflater, container, savedInstanceState );*/

        view = inflater.inflate( R.layout.fragment_first, container, false );

        return view;

    }



    @OnClick(R.id.btn_fregment_first)
    public void onViewClicked() {

        Toast.makeText( getActivity( ), "First Fragment", Toast.LENGTH_LONG ).show( );


    }
}
