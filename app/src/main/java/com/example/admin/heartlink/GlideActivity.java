package com.example.admin.heartlink;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GlideActivity extends AppCompatActivity {

    @BindView(R.id.tvtitle_manu)
    TextView tvtitleManu;
    @BindView(R.id.nav_toolbar)
    Toolbar navToolbar;
    @BindView(R.id.iv_glide)
    ImageView ivGlide;
    @BindView(R.id.iv2d_glide)
    ImageView iv2dGlide;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.glide_activity );
        ButterKnife.bind( this );

        String glide = "{\"result\": {\n" +
                "        \"userId\": \"806\",\n" +
                "        \"name\": \"Joseph Chuwa\",\n" +
                "        \"email\": \"harmistest@gmail.com\",\n" +
                "        \"phone\": \"0788888888\",\n" +
                "        \"username\": \"0788888888\",\n" +
                "        \"userpoint\": 0,\n" +
                "        \"profileImage\": \"http://dli.tanzmed.co.tz/images/avatar/thumb_c6e70c50e459429513ed4b96.jpg\",\n" +
                "        \"badgeCount\": 0,\n" +
                "        \"registerDate\": \"2018-04-11 14:02:50\",\n" +
                "        \"userprogress\": 85,\n" +
                "        \"notiCount\": \"10\",\n" +
                "        \"enableNotification\": \"1\",\n" +
                "        \"isenableReminder\": \"1\",\n" +
                "        \"userLocation\": \"\",\n" +
                "        \"childId\": 9\n" +
                "    }\n}";

        try {
            JSONObject jsonObject = new JSONObject( glide );
            JSONObject imageobject = jsonObject.getJSONObject( "result" );
            Log.e( "aaa", imageobject.getString( "profileImage" ) );
            String url = imageobject.getString( "profileImage" );

            Glide.with( this )
                    .load( url)
                    .asBitmap()
                    .into( new SimpleTarget <Bitmap>(100,100 ) {
                        @Override
                        public void onResourceReady(Bitmap image, GlideAnimation glideAnimation) {
                            saveImage(image);

                        }
                    } );



        } catch (JSONException e) {
            e.printStackTrace( );
        }


    }

    private String saveImage(Bitmap image) {

        String saveImagePath = null;

        String imageFileName = "JPEG_" + "FILE_NAME" + ".jpg";
        File storageDir = new File( Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES )
                + "/Image" );
        boolean success = true;
        if (!storageDir.exists( )) {

            success = storageDir.mkdir( );

        }
        if (success) {
            File imagefile = new File( storageDir, imageFileName );
            saveImagePath = imagefile.getAbsolutePath( );
            try {
                OutputStream fOut = new FileOutputStream( imagefile );
                image.compress( Bitmap.CompressFormat.JPEG, 100, fOut );
                fOut.close( );
            } catch (Exception e) {
                e.printStackTrace( );
            }
            // Add the image to the system gallery
            galleryAddPic( saveImagePath );
            Toast.makeText( this, "IMAGE SAVED", Toast.LENGTH_LONG ).show( );

        }
        return saveImagePath;
    }

        private void galleryAddPic(String imagePath){
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File f = new File(imagePath);
            Uri contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            sendBroadcast(mediaScanIntent);

        }
    }