package com.example.admin.heartlink.Adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View.OnClickListener;

import com.example.admin.heartlink.GridActivity;
import com.example.admin.heartlink.R;
import com.example.admin.heartlink.SecondActivity;

import java.util.ArrayList;

public class Gridadapter extends RecyclerView.Adapter<Gridadapter.MyviewHolder> {

    ArrayList celebritiesImage;
    ArrayList celebritiesName;
    GridActivity gridActivity;

    public Gridadapter(GridActivity gridActivity, ArrayList celebritiesImage, ArrayList celebritiesName) {

        this.celebritiesImage=celebritiesImage;
        this.celebritiesName=celebritiesName;
        this.gridActivity=gridActivity;
    }


    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from( parent.getContext() ).inflate( R.layout.grid_layout,parent,false );
        MyviewHolder vh=new MyviewHolder( view );
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, final int position) {
        holder.name.setText( ( CharSequence ) celebritiesName.get( position ) );
        holder.image.setImageResource( ( Integer ) celebritiesImage.get( position ) );

       holder.itemView.setOnClickListener( new View.OnClickListener( ) {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(gridActivity, SecondActivity.class);
               intent.putExtra("image", ( Integer ) celebritiesImage.get(position) );
               gridActivity.startActivity(intent );
           }
       } );

    }


    @Override
    public int getItemCount() {
        return celebritiesName.size();

    }


    public class MyviewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;
        public MyviewHolder(View itemView) {
            super( itemView );

            name=itemView.findViewById( R.id.tv_grideview );
            image=itemView.findViewById( R.id.iv_gridview );
        }
    }
}
