package com.example.admin.heartlink.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.heartlink.Method.ImgTxt;
import com.example.admin.heartlink.R;
import com.example.admin.heartlink.RecyclerView_OnClickListener.OnClickListner;

import java.util.ArrayList;
import java.util.List;

public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.HorizontalHolder>{

    private final List <ImgTxt> horizontalModels;
    private List<ImgTxt> horizontal ;
    private Context context;

    public HorizontalAdapter(List<ImgTxt> horizontalModels) {
        this.horizontalModels=horizontalModels;
    }

    @NonNull
    @Override
    public HorizontalHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.horizontal_layout, parent, false );
        return new HorizontalHolder( itemView );
    }

    @Override
    public void onBindViewHolder(@NonNull HorizontalHolder holder, int position) {
        ImgTxt horizontalModel = horizontalModels.get( position );
        holder.userimage.setImageResource( horizontalModel.getImage() );
    }

    @Override
    public int getItemCount() {
        return horizontalModels.size();
    }

    public class HorizontalHolder extends RecyclerView.ViewHolder {

        public ImageView userimage;
        public TextView username;
        public HorizontalHolder(View itemView) {
            super( itemView );

            userimage = itemView.findViewById( R.id.civ_horizontal );
            username = itemView.findViewById( R.id.tv_horizontal );
        }
    }
}
