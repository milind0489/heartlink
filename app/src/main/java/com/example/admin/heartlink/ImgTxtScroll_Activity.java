package com.example.admin.heartlink;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.heartlink.Adapter.ImgTxtScrollAdapter;
import com.example.admin.heartlink.Method.ImgTxt;
import com.example.admin.heartlink.ClickListner.RandomNumberGenerator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImgTxtScroll_Activity extends AppCompatActivity {

    @BindView(R.id.tvtoolbar_title)
    TextView tvtoolbarTitle;
    @BindView(R.id.toolbar_recycleview)
    Toolbar toolbarRecycleview;
    @BindView(R.id.tool_recycleview)
    RecyclerView toolRecycleview;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    public static View view;
    public static RecyclerView listRecycleView;
    public static ArrayList<ImgTxt> listArrayList = new ArrayList<>();
    public static ImgTxtScrollAdapter imgTxtScrollAdapter;
    String logger = "MyApp";

    private static int[] images = {
            R.drawable.images,R.drawable.images,R.drawable.images,R.drawable.images,R.drawable.images,
            R.drawable.images,R.drawable.images,R.drawable.images,R.drawable.images
    };

        String[] getUsername, getUserdetail;
        /*public RelativeLayout progressBar;*/
        public static LinearLayoutManager mLayoutManager;

        // Variables for scroll listener
        private boolean userScrolled = true;
        int pastVisiblesItems, visibleItemCount, totalItemCount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.img_txt_scroll_activity );
        ButterKnife.bind( this );
        setSupportActionBar( toolbarRecycleview );
        init();
        populatRecyclerView();
        implementScrollListener();
    }

    private void implementScrollListener() {

        toolRecycleview.addOnScrollListener( new RecyclerView.OnScrollListener( ) {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged( recyclerView, newState );
                if(newState== AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    userScrolled=true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled( recyclerView, dx, dy );

                // Here get the child count, item count and visible items
                // from layout manager
                //Log.e( logger,"event 1" );
                visibleItemCount = mLayoutManager.getChildCount();
                //Toast.makeText( getApplicationContext(),"visible items: " + visibleItemCount, Toast.LENGTH_SHORT ).show();
                totalItemCount = mLayoutManager.getItemCount();
                //Toast.makeText( getApplicationContext(),"total items: " +totalItemCount, Toast.LENGTH_SHORT ).show();
                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                //Toast.makeText( getApplicationContext(),"past visible items: ", Toast.LENGTH_SHORT ).show();
                //Log.e(logger,"test run");

                // Now check if userScrolled is true and also check if
                // the item is end then update recycler view and set
                // userScrolled to false
                if (userScrolled && (visibleItemCount + pastVisiblesItems) == totalItemCount) {
                    userScrolled = false;

                    updateRecyclerView();
                }
                
            }
        } );

    }

    private void updateRecyclerView() {

        toolRecycleview=findViewById( R.id.tool_recycleview );
        progressBar.setVisibility( View.VISIBLE );
        Log.e( logger,"test run" );
        // Handler to show refresh for a period of time you can use async task
        // while commnunicating serve
        new Handler(  ).postDelayed( new Runnable( ) {
            @Override
            public void run() {

                for (int i=0;i<5;i++){
                     int value = new RandomNumberGenerator().RandomGenerator();
                    listArrayList.add( new ImgTxt( images[value], getUsername[value], getUserdetail[value] ) );

                }

                Toast.makeText( getApplicationContext(),"total items: " +totalItemCount, Toast.LENGTH_SHORT ).show();
                imgTxtScrollAdapter.notifyDataSetChanged();
                Toast.makeText( getApplicationContext(), "Items Updated.", Toast.LENGTH_SHORT ).show();
                progressBar.setVisibility( View.GONE );


            }
        } ,3000);

    }

    private void populatRecyclerView() {

        listArrayList=new ArrayList <ImgTxt>(  );

        for (int i = 0; i < getUsername.length; i++) {
            listArrayList.add( new ImgTxt(images[i], getUsername[i], getUserdetail[i] ) );

        }

        imgTxtScrollAdapter = new ImgTxtScrollAdapter( getApplicationContext(), listArrayList );
        toolRecycleview.setAdapter( imgTxtScrollAdapter );
        imgTxtScrollAdapter.notifyDataSetChanged();

    }

    private void init(){

            progressBar=findViewById( R.id.progressBar );
            getUsername = getResources().getStringArray( R.array.title );
            getUserdetail = getResources().getStringArray( R.array.location );
            mLayoutManager=new LinearLayoutManager( getApplicationContext() );
            toolRecycleview=findViewById( R.id.tool_recycleview );
            toolRecycleview.setLayoutManager( mLayoutManager );
            toolRecycleview.setHasFixedSize( true );

        }
}
