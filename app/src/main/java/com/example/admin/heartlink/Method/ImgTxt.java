package com.example.admin.heartlink.Method;

import android.content.Intent;

public class ImgTxt  {


    private String imgtitle,imgname;
    private int image;

    public ImgTxt(int i, String milind){
            }

    public ImgTxt(int image, String imgtitle, String imgname){

        this.image=image;
        this.imgtitle=imgtitle;
        this.imgname=imgname;
    }

    public String getImgtitle() {
        return imgtitle;
    }

    public void setImgtitle(String imgtitle) {
        this.imgtitle = imgtitle;
    }

    public String getImgname() {
        return imgname;
    }

    public void setImgname(String imgname) {
        this.imgname = imgname;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
