package com.example.admin.heartlink;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.heartlink.Utils.MYsharedPreferences;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class Profile_Activity extends AppCompatActivity {


    @BindView(R.id.rlsub_profile_setting)
    Button rlsubProfileSetting;
    @BindView(R.id.rlsub_profile)
    RelativeLayout rlsubProfile;
    @BindView(R.id.rlsub_profile_useriv)
    CircleImageView rlsubProfileUseriv;
    @BindView(R.id.rlsub_profile_userimage)
    RelativeLayout rlsubProfileUserimage;
    @BindView(R.id.rlsub_profile_detail_tvName)
    TextView rlsubProfileDetailTvName;
    @BindView(R.id.rlsub_profile_detail_tvAddress)
    TextView rlsubProfileDetailTvAddress;
    @BindView(R.id.rlsub_profile_detail_tvPhoneNumber)
    TextView rlsubProfileDetailTvPhoneNumber;
    @BindView(R.id.rlsub_profile_detail)
    RelativeLayout rlsubProfileDetail;
    @BindView(R.id.rl_profile)
    RelativeLayout rlProfile;
    @BindView(R.id.clear)
    Button btnclear;

    private String text;

    private MYsharedPreferences sharedPreference;

    Activity context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.profile_activity );
        ButterKnife.bind( this );
        sharedPreference=new MYsharedPreferences();
        final Intent intent = getIntent();
        String name = getIntent().getStringExtra( "username" );
        rlsubProfileDetailTvName.setText( name );
        rlsubProfileDetailTvName.setTextSize( 25 );
        String phone = getIntent().getStringExtra( "phonenumber" );
        rlsubProfileDetailTvPhoneNumber.setText( phone );
        rlsubProfileDetailTvPhoneNumber.setTextSize( 25 );
        String address1 = getIntent().getStringExtra( "address" );
        rlsubProfileDetailTvAddress.setText( address1 );
        rlsubProfileDetailTvAddress.setTextSize( 25 );


        rlsubProfileSetting.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( Profile_Activity.this, EditProfileActivity.class );
                String name = rlsubProfileDetailTvName.getText().toString();
                intent.putExtra( "username", name );
                String phone = rlsubProfileDetailTvPhoneNumber.getText().toString();
                intent.putExtra( "phonenumber", phone );
                String address = rlsubProfileDetailTvAddress.getText().toString();
                intent.putExtra( "address", address );
                startActivity( intent );
            }
        } );
        btnclear.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text=rlsubProfileDetailTvName.getText().toString();
                sharedPreference.clear( context);
                Toast.makeText( getApplicationContext(),"clear data",Toast.LENGTH_LONG).show();
                Intent intent1 =new Intent( Profile_Activity.this,Login_Activity.class );
                startActivity( intent1 );
            }
        } );
    }

    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setCancelable( false );
        builder.setMessage( "Do you want to exit?" );
        builder.setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Login_Activity.activity != null) {
                    Login_Activity.activity.finish();
                }
                finish();
            }
        } );
        builder.setNegativeButton( "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        } );
        builder.show();

    }

}


