package com.example.admin.heartlink.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class MYsharedPreferences {


    public static final String PREFS_NAME = "AOP_PREFS";
    public static final String PREFS_KEY = "AOP_PREFS_String";
    public static final String PREFS_password = "AOP_PREFS";
    public MYsharedPreferences(){

            super();
    }
    public void save(Context context,String text){

        SharedPreferences settings;

        SharedPreferences.Editor editor;
        settings=context.getSharedPreferences( PREFS_NAME,Context.MODE_PRIVATE );
        editor=settings.edit();

        editor.putString( PREFS_KEY , text );
        editor.commit();

    }
    public String getValue(Context context){

        SharedPreferences settings;
        String text;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(PREFS_KEY, null);
        return text;
    }
    public void clear(Context context){

        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings=context.getSharedPreferences( PREFS_NAME,Context.MODE_PRIVATE );
        editor=settings.edit();
        editor.clear();
        editor.commit();

    }
    public void removeValue(Context context){

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings=context.getSharedPreferences( PREFS_NAME,Context.MODE_PRIVATE );
        editor=settings.edit();
        editor.remove( PREFS_KEY );
        editor.commit();
    }
}
